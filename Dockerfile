FROM node:12

RUN npm i -g typescript ts-node nodemon

WORKDIR /var/www/project
ADD package.json /var/www/project
ADD yarn.lock /var/www/project
RUN yarn install

ADD . /var/www/project

EXPOSE 3000

CMD ["docker/start.sh"]

